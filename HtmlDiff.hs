module HtmlDiff(getDiffFormatted) where

import qualified Data.Algorithm.Diff as Diff

processOneLine diff =
    foldl (\acc x -> acc ++ diffToHtml x) [] diff
    where diffToHtml (Diff.S, str) = "<span class='add'>" ++ str ++ "</span>"
          diffToHtml (Diff.F, str) = "<span class='del'>" ++ str ++ "</span>"
          diffToHtml (_, str) = str

getDiffFormatted :: String -> String -> Maybe String
getDiffFormatted left right =
    let diff = Diff.getGroupedDiff left right in
        case filter (\(d, s) -> d /= Diff.B) diff of
            [] -> Nothing
            _  -> Just $ processOneLine diff


