<?php

$postBody = file_get_contents('php://input');

$str = sprintf("%s %s: %s\n", date('Ymd His'), $_SERVER['HTTP_USER_AGENT'], $postBody);

$h = fopen(__DIR__.'/diff.txt', 'a');
fwrite($h, $str);
fclose($h);

echo "OK";