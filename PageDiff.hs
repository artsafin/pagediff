import qualified Network.HTTP as Http
import qualified System.Environment as Env
import qualified System.IO as Io
import qualified System.IO.Strict as Sio
import HtmlDiff;
import System.Directory;

type Url = String
type ProgArgs = [String]

main = do
	args <- Env.getArgs
	case args of
		[_, _, _] -> doCoolStuff args
		[uf,ut] -> putStrLn "Using sample.txt as a sample file" >>= (\_ -> doCoolStuff [uf, ut, "sample.txt"])
		_ -> printUsage
	
printUsage = do
	prog <- Env.getProgName
	putStrLn $ "usage: " ++ prog ++ " <url to track> <url to post diff result> [<file mirroring current state of tracked url>]"

doCoolStuff :: ProgArgs -> IO ()
doCoolStuff [urlFrom, urlTo, sampleFile] = do
	right <- getUrlContent urlFrom
	sampleExist <- doesFileExist sampleFile
	left <- if sampleExist then Sio.readFile sampleFile else return []

	Io.withFile sampleFile Io.WriteMode (\h -> Io.hPutStr h right)

	let diffRes = getDiffFormatted left right

	case diffRes of
		Just diff -> do
			putStrLn "Diff found"
			postDiffToUrl urlTo diff
		Nothing ->
			putStrLn "No difference found"

getUrlContent :: Url -> IO String
getUrlContent url =
	Http.simpleHTTP (Http.getRequest url) >>= Http.getResponseBody

postDiffToUrl :: Url -> String -> IO ()
postDiffToUrl url diffText = do
	Http.simpleHTTP (Http.postRequestWithBody url "text/plain" diffText)
	return ()
